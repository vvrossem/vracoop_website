# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api


class VracoopWebsite(models.Model):
    _inherit = 'website'

    type_livraison = fields.Boolean(
        "Est de type livraison",
        compute='_compute_type_livraison')

    @api.model
    def _compute_type_livraison(self):
        for website in self:
            delivery_carriers = self.env['delivery.carrier'].sudo().search([
                ('website_published', '=', True), 
                ('type_carrier', '=', 'livraison')])
            if delivery_carriers:
                website.type_livraison = True
            else:
                website.type_livraison = False
